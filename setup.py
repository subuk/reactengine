# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requires = [
    'gunicorn',
    'django>=1.9,<2.0',
    'psycopg2',
    'pillow',
    'requests',
    'beautifulsoup4',
    'progressbar',
    'dateparser',
    'django-pglocks',
]

develop_requires = [
    'ipdb',
]

setup(
    name='reactengine',
    version='0.5.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    install_requires=requires,
    zip_safe=False,
    extras_require={
        'develop': develop_requires,
    },
    entry_points={
        'console_scripts': [
            'reactengine = reactengine.manage:main',
            'reactengine-server = gunicorn.app.wsgiapp:run',
        ]
    },
)
