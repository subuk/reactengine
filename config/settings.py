# Reactengine settings overrides

DEBUG = False

STATIC_ROOT = '/opt/reactengine/'
MEDIA_ROOT = '/var/lib/reactengine/media/'
MEDIA_URL = '/media/'
STATIC_URL = '/static/'

FFMPEG_BIN = '/opt/reactengine/bin/ffmpeg'
