#!/bin/sh

pypath="$1"
realpath="$2"
writeto="$3"

cat > $writeto <<EOF
#!/bin/sh
export REACTENGINE_SETTINGS_OVERRIDE=/etc/reactengine/settings.py
export PYTHONPATH=$pypath
exec $realpath \$@
EOF
chmod +x $writeto
