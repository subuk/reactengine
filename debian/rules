#!/usr/bin/make -f
# -*- makefile -*-

ROOT_DIR = $(shell pwd)

PROJECT_NAME = reactengine
DEB_DH_INSTALLINIT_ARGS = --no-start
DEPENDS_DIR_NAME = embed
SCRIPTS_DIR_NAME = bin
DEPENDS_DIR_PATH = /opt/$(PROJECT_NAME)/$(DEPENDS_DIR_NAME)
GENERATED_ENTRYPOINTS_PATH = $(DEPENDS_DIR_NAME)/__entry-points

PY_METADATA_PATH = src/$(PROJECT_NAME).egg-info/
WRAP_DEB_BIN_PATH = ../../wrap-deb-bin.sh

include /usr/share/cdbs/1/rules/debhelper.mk

clean::
	rm -rf bin/ build/ dist/ embed/ __static/ staticfiles.json

fixversion:
	# Fix version in setup.py
	sed -i "s/\(__version__ = \).*/\1'$(DEB_VERSION)'/" $(CURDIR)/src/$(PROJECT_NAME)/__init__.py

build: fixversion
	# Setup package metadata
	python setup.py egg_info
	date -R > $(PY_METADATA_PATH)/X_buildtime
	git rev-parse --short HEAD > $(PY_METADATA_PATH)/X_githash

	# Make source distribution
	python setup.py sdist

	# Install project package and all dependencies
	mkdir -p $(DEPENDS_DIR_NAME) $(GENERATED_ENTRYPOINTS_PATH)
	pip install --compile \
	    --target $(DEPENDS_DIR_NAME) \
	    --install-option="--install-scripts=$(ROOT_DIR)/$(GENERATED_ENTRYPOINTS_PATH)" \
	    $(CURDIR)/dist/$(PROJECT_NAME)-$(shell python setup.py --version|tr '~' '-').tar.gz

	# Django collectstatic
	echo "STATIC_ROOT = '$(CURDIR)/__static'" > $(CURDIR)/__buildsettings.py
	REACTENGINE_SETTINGS_OVERRIDE=$(CURDIR)/__buildsettings.py PYTHONPATH=$(DEPENDS_DIR_NAME) \
		$(GENERATED_ENTRYPOINTS_PATH)/$(PROJECT_NAME) collectstatic --noinput \
		--ignore *.html --ignore *.map
	rm -f $(CURDIR)/__buildsettings.py
	mv $(CURDIR)/__static/staticfiles.json $(CURDIR)/

	# Wrap generated entry-points
	mkdir -p $(SCRIPTS_DIR_NAME)
	cd $(GENERATED_ENTRYPOINTS_PATH) && find . -maxdepth 1 -type f -executable |xargs -n1 -I{} $(WRAP_DEB_BIN_PATH) $(DEPENDS_DIR_PATH) /opt/$(PROJECT_NAME)/$(GENERATED_ENTRYPOINTS_PATH)/{} ../../$(SCRIPTS_DIR_NAME)/{}
