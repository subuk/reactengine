from django.views.generic import ListView, DetailView, MonthArchiveView, YearArchiveView
from reactengine.models import Picture


class PictureListView(ListView):
    queryset = Picture.published.all()
    paginate_by = 10
    context_object_name = 'pictures'

picture_list_view = PictureListView.as_view()


class PictureDetailView(DetailView):
    model = Picture

picture_detail_view = PictureDetailView.as_view()


class PictureMonthArchiveView(MonthArchiveView):
    queryset = Picture.published.all()
    date_field = "source_date_created"
    template_name = 'reactengine/picture_list.html'
    context_object_name = 'pictures'
    paginate_by = 10
