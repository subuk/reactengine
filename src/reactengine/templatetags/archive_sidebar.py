import datetime
from django import template
from reactengine.models import Picture

register = template.Library()


def month_year_iter(start_month, start_year, end_month, end_year):
    ym_start = 12 * start_year + start_month - 1
    ym_end = 12 * end_year + end_month
    for ym in range(ym_start, ym_end):
        y, m = divmod(ym, 12)
        yield datetime.date(year=y, month=m + 1, day=1)


@register.inclusion_tag('archive_sidebar.html')
def archive_sidebar():
    first_post = Picture.published.all().reverse()[0]
    last_post = Picture.published.all()[0]

    dates = list(month_year_iter(
        first_post.source_date_created.month,
        first_post.source_date_created.year,
        last_post.source_date_created.month,
        last_post.source_date_created.year
    ))

    return {
        "archive_dates": reversed(dates),
    }
