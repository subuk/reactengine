import mimetypes
import requests
import tempfile
import subprocess
import progressbar as pb
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.core.files.base import File
from django.db.models import Q
from django_pglocks import advisory_lock
from reactengine.models import Picture


class Command(BaseCommand):

    def convert_gif_to_webm(self, input_file):
        output = tempfile.NamedTemporaryFile()
        args = [
            settings.FFMPEG_BIN, '-y',
            '-i', input_file,
            '-c:v', 'libvpx', '-crf', '12', '-b:v', '500K', '-f', 'webm', output.name,
        ]
        self.stdout.write("Executing ffpmeg: " + " ".join(args))
        proc = subprocess.Popen(args, stderr=subprocess.PIPE)
        if proc.wait() != 0:
            raise CommandError(proc.stderr.read())
        return output

    def convert_gif_to_mp4(self, input_file):
        output = tempfile.NamedTemporaryFile()
        args = [
            settings.FFMPEG_BIN, '-y',
            '-i', input_file,
            '-c:v', 'libx264', '-pix_fmt', 'yuv420p', '-movflags', '+faststart',
            '-vf', 'scale=trunc(iw/2)*2:trunc(ih/2)*2', '-f', 'mp4', output.name,
        ]
        self.stdout.write("Executing ffpmeg: " + " ".join(args))
        proc = subprocess.Popen(args, stderr=subprocess.PIPE)
        if proc.wait() != 0:
            raise CommandError(proc.stderr.read())
        return output

    def create_poster(self, input_file):
        output = tempfile.NamedTemporaryFile(suffix='.png')
        args = [
            settings.FFMPEG_BIN, '-y',
            '-i', input_file,
            '-ss', '00:00:01',
            '-vframes', '1', output.name,
        ]
        self.stdout.write("Executing ffpmeg: " + " ".join(args))
        proc = subprocess.Popen(args, stderr=subprocess.PIPE)
        if proc.wait() != 0:
            raise CommandError(proc.stderr.read())
        return output

    def process_picture(self, picture):
        picture_basename = picture.caption.lower().replace(' ', '_')

        if not picture.source:
            response = requests.get(picture.source_image_url, stream=True)
            response.raise_for_status()
            source_ext = mimetypes.guess_extension(response.headers['content-type'])
            self.stdout.write("Downloading " + picture.source_image_url)
            widgets = [pb.Percentage(), u' ', pb.ETA(), u' ', pb.Bar()]
            pbar = pb.ProgressBar(widgets=widgets, maxval=1000000000)
            if 'Content-Length' in response.headers:
                pbar.maxval = int(response.headers['Content-Length'].strip())
            pbar.start()

            picture.source = File(
                file=tempfile.TemporaryFile(),
                name=picture_basename + source_ext
            )

            downloaded = 0
            for buf in response.iter_content(4096):
                if buf:
                    downloaded += len(buf)
                    picture.source.write(buf)
                    pbar.update(downloaded)
            pbar.finish()
            picture.save()

        if not picture.video_webm:
            picture.video_webm = File(
                file=self.convert_gif_to_webm(picture.source.path),
                name=picture_basename + u'.webm'
            )
            picture.save()

        if not picture.video_mp4:
            picture.video_mp4 = File(
                file=self.convert_gif_to_mp4(picture.source.path),
                name=picture_basename + u'.mp4'
            )
            picture.save()

        if not picture.poster:
            picture.poster = File(
                file=self.create_poster(picture.source.path),
                name=picture_basename + u'.png'
            )
            picture.save()

        if not picture.is_published:
            picture.is_published = True
            picture.save()

    def handle(self, *args, **options):
        lock = advisory_lock('convert_and_publish_pictures', wait=False)
        with lock as acquired:
            if not acquired:
                self.stderr.write("Error: another process already running")
                return
            condition = Q(video_webm='') | Q(video_mp4='') | Q(source='') | Q(poster='')
            for picture in Picture.objects.filter(condition).order_by('-id'):
                self.stdout.write(u"processing picture '{}'".format(picture))
                self.process_picture(picture)
