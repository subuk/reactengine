from django.core.management.base import BaseCommand

from reactengine.models import Picture
from reactengine.parser import devopsreactions


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--start-page', type=int, default=1)

    def handle(self, *args, **options):
        picstream = devopsreactions.parse(options['start_page'])
        for picture_data in picstream:
            exists = Picture.objects.filter(
                caption=picture_data['caption'],
                source_image_url=picture_data['source_image_url'],
            ).exists()
            if exists:
                self.stdout.write(u'picture "{}" already exist, stopping'.format(
                    picture_data['caption']
                ))
                return
            Picture.objects.create(**picture_data)
