
from django.conf import settings


def metrics_code(request):
    return {
        "METRICS_CODE": settings.METRICS_CODE,
    }
