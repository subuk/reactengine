import re
import logging
import bs4
import requests
import dateparser
from django.utils.timezone import UTC

logger = logging.getLogger(__name__)

LIKE_COUNT_RE = re.compile(ur'^(\d+)')


def extract_pictures(raw):
    html = bs4.BeautifulSoup(raw, 'html.parser')
    previous_caption = "__FIRST__"
    pictures = html.find_all('div', class_='item')
    if not pictures:
        yield None
    for post in pictures:
        picture = {}

        post_title = post.find(class_='post_title')
        if not post_title:
            logger.info(
                'no caption found for post after "%s", skipping',
                previous_caption,
            )
            continue

        a = post_title.find('a')
        picture['caption'] = previous_caption = a.text
        picture['source_url'] = a.attrs['href']

        img = post.find('img')
        if not img:
            logger.info(
                'no image found for post "%s", skipping',
                picture['caption']
            )
            continue
        picture['source_image_url'] = img.attrs['src']

        date_text = post.find(class_='post_date').text
        date = dateparser.parse(date_text).replace(tzinfo=UTC())
        picture['source_date_created'] = date

        like_count_el = post.find(class_="num_notes")
        if like_count_el:
            picture['like_count'] = int(LIKE_COUNT_RE.findall(like_count_el.text)[0])

        author_el = post.find(class_="item_content").find("p")
        if author_el:
            picture['author'] = author_el.text[3:]

        logger.info(u'found image %(caption)s (%(source_image_url)s)', picture)
        yield picture


def parse(start_page):
    current_page = start_page
    stop = False
    while not stop:
        url = 'http://devopsreactions.tumblr.com/page/{}'.format(current_page)
        logger.info('parsing url %s', url)
        response = requests.get(url)
        for picture in extract_pictures(response.content):
            if picture is None:
                stop = True
                break
            yield picture
        current_page += 1
