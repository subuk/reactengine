from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from reactengine import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.picture_list_view, name='picture_list'),
    url(r'^(?P<pk>\d+)/$', views.picture_detail_view, name='picture_detail'),
    url(r'^(?P<year>[0-9]{4})-(?P<month>[-a-z]+)/$',
        views.PictureMonthArchiveView.as_view(),
        name="picture_archive_month"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
