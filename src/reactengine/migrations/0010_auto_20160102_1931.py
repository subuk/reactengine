# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-02 19:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactengine', '0009_auto_20160102_1546'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='video_mp4',
            field=models.FileField(blank=True, upload_to=b'videos_mp4'),
        ),
        migrations.AlterField(
            model_name='picture',
            name='source',
            field=models.FileField(upload_to=b'source'),
        ),
    ]
