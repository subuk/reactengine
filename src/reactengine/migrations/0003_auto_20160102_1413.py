# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-02 14:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactengine', '0002_picture_date_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='published',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='picture',
            name='source_image_url',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='picture',
            name='source_url',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='picture',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
