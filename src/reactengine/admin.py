
from django.contrib import admin
from reactengine.models import Picture, Tag


@admin.register(Picture)
class PictureAdmin(admin.ModelAdmin):
    search_fields = ('caption',)
    filter_horizontal = ('tags',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass
