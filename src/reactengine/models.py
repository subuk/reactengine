from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class PublishedManager(models.Manager):
    def get_queryset(self, *args, **kw):
        return super(PublishedManager, self).get_queryset(*args, **kw).filter(
            is_published=True
        )


class Picture(models.Model):
    source = models.FileField(upload_to='source')
    video_webm = models.FileField(upload_to='videos_webm', blank=True)
    video_mp4 = models.FileField(upload_to='videos_mp4', blank=True)
    poster = models.FileField(upload_to='posters', blank=True)
    caption = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True)
    like_count = models.IntegerField(default=0)
    tags = models.ManyToManyField(Tag, blank=True)

    source_url = models.URLField(blank=True)
    source_image_url = models.URLField(blank=True)
    source_date_created = models.DateTimeField(blank=True, null=True)

    date_created = models.DateTimeField(auto_now_add=True, editable=True)
    is_published = models.BooleanField(default=False)

    objects = models.Manager()
    published = PublishedManager()

    def __unicode__(self):
        return self.caption

    class Meta:
        ordering = ['-source_date_created']
